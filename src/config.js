const description =
  "This is the description of my NFT project";
const baseUri = "https://my-nft";

const layersOrder = [
    { name: "background" },
    { name: "body" },
    { name: "arms" },
    { name: "legs" },
    { name: "eyes" },
    { name: "mouth" },
];

const format = {
  width: 512,
  height: 512,
};

const background = {
  generate: true,
  brightness: "80%",
};

const uniqueDnaTorrance = 10000;

const editionSize = 10;

module.exports = {
  layersOrder,
  format,
  editionSize,
  baseUri,
  description,
  background,
  uniqueDnaTorrance,
};
